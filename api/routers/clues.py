from database.fake_clue_data_provider import FakeClueDataProvider
from database.postgres_clue_data_provider import PostgresClueDataProvider
from database.models import CluesListResponse, ClueResponse, HealthResponse
from fastapi import APIRouter

clue_router = APIRouter()
db = PostgresClueDataProvider()


@clue_router.get("/clues/health", response_model=HealthResponse)
async def health():
    health = await db.is_healthy()

    return HealthResponse(
        healthy=health,
        provider=str(db.__class__.__name__),
    )


@clue_router.get("/clues/{clue_id}", response_model=ClueResponse)
async def get_clue(clue_id: int):
    clue = await db.get_clue(clue_id=clue_id)

    return ClueResponse(
        data=clue,
        provider=str(db.__class__.__name__),
    )


@clue_router.get("/random-clue", response_model=ClueResponse)
async def get_random_clue(valid: bool = True):
    clue = await db.get_random_clue(valid=valid)

    return ClueResponse(
        data=clue,
        provider=str(db.__class__.__name__),
    )


@clue_router.get("/clues", response_model=CluesListResponse)
async def get_all_clues(value: int | None = None, page: int = 0):
    all_clues = await db.get_all_clues(value=value, page=page)

    return CluesListResponse(
        data=all_clues,
        provider=str(db.__class__.__name__),
    )


@clue_router.delete("/clues/{clue_id}", response_model=ClueResponse)
async def delete_clue(clue_id: int):
    clue = await db.delete_clue(clue_id=clue_id)

    return ClueResponse(
        data=clue,
        provider=str(db.__class__.__name__),
    )
