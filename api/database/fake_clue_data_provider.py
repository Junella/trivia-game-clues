from database.clue_data_provider import ClueDataProvider
from database.models import Clue, Category


def fake_clue():
    return Clue(
        id=12345,
        answer="fake",
        question="fake",
        value=12345,
        invalid_count=12345,
        canon=False,
        category=Category(
            id=12345,
            title="fake",
            canon=True,
        ),
    )


class FakeClueDataProvider(ClueDataProvider):
    async def is_healthy(self):
        return True

    async def get_clue(self, clue_id: int):
        return fake_clue()

    async def get_random_clue(self, valid: bool):
        return fake_clue()

    async def get_all_clues(self, value: (int | None), page: int):
        return [fake_clue(), fake_clue(), fake_clue()]

    async def delete_clue(self, clue_id: int):
        return fake_clue()
