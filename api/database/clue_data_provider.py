from abc import ABC, abstractmethod


class ClueDataProvider(ABC):
    @abstractmethod
    async def is_healthy(self):
        pass

    @abstractmethod
    async def get_clue(self, clue_id: int):
        pass

    @abstractmethod
    async def get_random_clue(self, valid: bool):
        pass

    @abstractmethod
    async def get_all_clues(self, value: (int | None), page: int):
        pass

    @abstractmethod
    async def delete_clue(self, clue_id: int):
        pass
