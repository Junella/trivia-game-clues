from pydantic import BaseModel


class Category(BaseModel):
    id: int
    title: str
    canon: bool


class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: Category
    canon: str


class ClueResponse(BaseModel):
    data: Clue
    provider: str


class CluesListResponse(BaseModel):
    data: list[Clue]
    provider: str


class HealthResponse(BaseModel):
    healthy: bool
    provider: str
