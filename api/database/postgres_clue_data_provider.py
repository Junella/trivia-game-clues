import psycopg
from database.clue_data_provider import ClueDataProvider
from database.models import Clue, Category


def postgres_clue(row, cursor):
    record = {}

    # Convert row array to look more like a dictionary
    for i, column in enumerate(cursor.description):
        record[column.name] = row[i]

    return Clue(
        id=record["clue_id"],
        answer=record["answer"],
        question=record["question"],
        value=record["value"],
        invalid_count=record["invalid_count"],
        canon=record["canon"],
        category=Category(
            id=record["category_id"],
            title=record["category_title"],
            canon=record["category_canon"],
        ),
    )


class PostgresClueDataProvider(ClueDataProvider):
    async def is_healthy(self):
        try:
            with psycopg.connect() as conn:
                with conn.cursor():
                    return True
        except:
            return False

    async def get_clue(self, clue_id: int) -> None | Clue:
        pass

    async def get_random_clue(self, valid: bool):
        pass

    async def get_all_clues(self, value: int | None, page: int):
        with psycopg.connect() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT
                        clues.id as clue_id,
                        clues.answer,
                        clues.question,
                        clues.value,
                        clues.invalid_count,
                        clues.canon,
                        categories.id as category_id,
                        categories.title as category_title,
                        categories.canon as category_canon
                    FROM clues INNER JOIN categories ON clues.category_id = categories.id
                    LIMIT 100
                    """
                )

                return [postgres_clue(row, cur) for row in cur.fetchall()]

    async def delete_clue(self, clue_id: int):
        pass
