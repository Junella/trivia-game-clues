from fastapi import FastAPI
from routers.clues import clue_router
from fastapi.responses import RedirectResponse

# So we don't have write "/api" everywhere else
app = FastAPI()
api = FastAPI()
app.mount("/api", api)


@app.get("/")
async def redirect_to_docs():
    return RedirectResponse("/api/docs")


api.include_router(clue_router)
